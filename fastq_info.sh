#!/bin/bash

##############################################################################################################
#                                                                                                            #
#  fastq_info: a BASH/AWK script to estimate standard statistics from FASTQ files                            #
#                                                                                                            #
   COPYRIGHT="Copyright (C) 2021 Institut Pasteur"                                                           #
#                                                                                                            #
#  This program  is free software:  you can  redistribute it  and/or modify it  under the terms  of the GNU  #
#  General Public License as published by the Free Software Foundation, either version 3 of the License, or  #
#  (at your option) any later version.                                                                       #
#                                                                                                            #
#  This program is distributed in the hope that it will be useful,  but WITHOUT ANY WARRANTY;  without even  #
#  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public  #
#  License for more details.                                                                                 #
#                                                                                                            #
#  You should have received a copy of the  GNU General Public License along with this program.  If not, see  #
#  <http://www.gnu.org/licenses/>.                                                                           #
#                                                                                                            #
#  Contact:                                                                                                  #
#   Alexis Criscuolo                                                            alexis.criscuolo@pasteur.fr  #
#   Genome Informatics & Phylogenetics (GIPhy)                                             giphy.pasteur.fr  #
#   Bioinformatics and Biostatistics Hub      research.pasteur.fr/team/bioinformatics-and-biostatistics-hub  #
#   Dpt. Biologie Computationnelle                     research.pasteur.fr/department/computational-biology  #
#   Institut Pasteur, Paris, FRANCE                                                     research.pasteur.fr  #
#                                                                                                            #
##############################################################################################################

##############################################################################################################
#                                                                                                            #
# ============                                                                                               #
# = VERSIONS =                                                                                               #
# ============                                                                                               #
#                                                                                                            #
  VERSION=3.0.211217ac                                                                                       #
# + now use FQreport to process the FASTQ file(s)                                                            #
# + replacing option '-v t' by '-v s' (i.e. summary)                                                         #
# + new tsv output, i.e. option -v t                                                                         #
# + new svg graphical outputs, i.e. options -v l or -v p                                                     #
#                                                                                                            #
# VERSION=2.0.210401ac                                                                                       #
# + estimates the distribution of the (expected) number of error(s) per HTS read                             #
# + adding option -r                                                                                         #
# + modified output tables                                                                                   #
#                                                                                                            #
# VERSION=1.0.210325ac                                                                                       #
#                                                                                                            #
##############################################################################################################
  
##############################################################################################################
#                                                                                                            #
#  ================                                                                                          #
#  = INSTALLATION =                                                                                          #
#  ================                                                                                          #
#                                                                                                            #
#  Check that the two mandatory programs gawk and FQreport are available in the $PATH.                       #
#                                                                                                            #
#  Give the execute permission to the script fastq_info.sh with the following command line:                  #
#                                                                                                            #
#   chmod +x fastq_info.sh                                                                                   #
#                                                                                                            #
#  Run fastq_info.sh with option  -c to check the  availability of the binaries  listed in the REQUIREMENTS  #
#  section (see below).                                                                                      #
#                                                                                                            #
##############################################################################################################

##############################################################################################################
#                                                                                                            #
# ================                                                                                           #
# = REQUIREMENTS =                                                                                           #
# ================                                                                                           #
#                                                                                                            #
# -- awk --------------------------------------------------------------------------------------------------  #
#                                                                                                            #
  AWK=awk;
  GAWK=gawk;
  [ $(command -v $GAWK) ] && AWK=$GAWK;
  if [ ! $(command -v $AWK) ]; then echo " no $AWK detected "           >&2 ; exit 1 ; fi
#                                                                                                            #
# -- FQreport ---------------------------------------------------------------------------------------------  #
#                                                                                                            #
  FQREPORT=fqreport;
  if [ ! $(command -v $FQREPORT) ]; then echo " no $FQREPORT detected " >&2 ; exit 1 ; fi
#                                                                                                            #
# -- gzip (www.gzip.org) ----------------------------------------------------------------------------------  #
#                                                                                                            #
  GZIP=gzip;
  GUNZIP="$GZIP -d -q -c";
#                                                                                                            #
# -- pigz (zlib.net/pigz/) --------------------------------------------------------------------------------  #
#                                                                                                            #
  PGZIP=pigz;
  PGUNZIP="$PGZIP -d -q -c";
#                                                                                                            #
# -- bzip2 (www.sourceware.org/bzip2) ---------------------------------------------------------------------  #
#                                                                                                            #
  BZIP=bzip2;
  BUNZIP="$BZIP -d -q -c";
#                                                                                                            #
# -- pbzip2 (compression.ca/pbzip2) -----------------------------------------------------------------------  #
#                                                                                                            #
  PBZIP=pbzip2;
  PBUNZIP="$PBZIP -d -q -c";
#                                                                                                            #
# -- DSRC 2 (sun.aei.polsl.pl/dsrc) -----------------------------------------------------------------------  #
#                                                                                                            #
  DSRC=dsrc;
  UNDSRC="$DSRC d -s";
#                                                                                                            #
# -- FQZCOMP 4 (github.com/jkbonfield/fqzcomp) ------------------------------------------------------------  #
#                                                                                                            #
  FQZCOMP=fqzcomp;
  UNFQZCOMP="$FQZCOMP -d";
#                                                                                                            #
# -- QUIP (github.com/dcjones/quip) -----------------------------------------------------------------------  #
#                                                                                                            #
  QUIP=quip;
  UNQUIP="$QUIP -d -c";
#                                                                                                            #
##############################################################################################################
  
#############################################################################################################
#                                                                                                           #
#  ================                                                                                         #
#  = FUNCTIONS    =                                                                                         #
#  ================                                                                                         #
#                                                                                                           #
# = mandoc() =============================================================================================  #
#   prints the doc                                                                                          #
#                                                                                                           #
mandoc() {
  echo -e "\n\033[1m fastq_info v$VERSION            $COPYRIGHT\033[0m";
  cat <<EOF

 USAGE:  fastq_info.sh  [options]  [<file1> <file2> ...] 

 Allowed file extensions (case insensitive):
  .bz
  .bz2
  .bzip
  .bzip2 ... considered as FASTQ-formatted files compressed using bzip2;
             decompressed  using  bunzip2  or pbzip2  (when available in 
             \$PATH)
  .dsrc
  .dsrc2 ... considered as  FASTQ-formatted  files compressed using DSRC 
             v2.0 (sun.aei.polsl.pl/dsrc);  decompressed using DSRC v2.0
             (when available in \$PATH)
  .fastq
  .fq ...... considered as uncompressed FASTQ-formatted files

  .fqz ..... considered  as   FASTQ-formatted  files   compressed  using 
             fqzcomp  v4  (github.com/jkbonfield/fqzcomp);  decompressed 
             using fqzcomp v4 (when available in \$PATH)
  .gz
  .gzip .... considered as FASTQ-formatted files  compressed using gzip;
             decompressed using gunzip or pigz (when available in \$PATH)

  .qp ...... considered as  FASTQ-formatted files  compressed using QUIP
             (github.com/dcjones/quip);  decompressed  using QUIP  (when 
             available in \$PATH)

 Options:
  -v <char>  output format specified by  one of the following character: 
               r   reduced table in txt format
               f   full table in txt format
               t   full table in tsv format
               s   summary in tsv format
               l   full report in svg format (landscape)
               p   full report in svg format (portrait)
             (default: r)
  -p <int>   Phred quality offset (default: 33)
  -d         DOS end-of-lines in input file(s) (default: not set)
  -t <int>   number of thread(s) for decompressing files (default: 1)
  -c         checks available tools (default: not set)
  -h         prints this help and exits

EOF
}
#                                                                                                           #
#############################################################################################################

#############################################################################################################
####                                                                                                     ####
#### INITIALIZING PARAMETERS AND READING OPTIONS                                                         ####
###                                                                                                     ####
#############################################################################################################
if [ $# -lt 1 ]; then mandoc ; exit 1 ; fi

FONTS="font-family=\"Arial, Helvetica, sans-serif\"";
FONTM="font-family=\"Consolas, Courier, monospace\"";

export LC_ALL=C;

OFFSET=33;
OUT="r";
NTHREADS=1;
CHECK=false;
DOS2UNIX=false;

while getopts p:t:v:cdh option
do
  case $option in
  p)  OFFSET=$OPTARG    ;;
  v)  OUT="$OPTARG"     ;;
  t)  NTHREADS=$OPTARG  ;;
  c)  CHECK=true        ;;
  d)  DOS2UNIX=true     ;;
  h)  mandoc ;  exit 0  ;;
  \?) mandoc ;  exit 1  ;;
  esac
done
shift "$(( $OPTIND - 1 ))"


if ! [[ $OFFSET =~ ^[0-9]+$ ]];                         then echo " incorrect value (option -p): $OFFSET"                 >&2 ; exit 1 ; fi
if    [ $OFFSET -ne 33 ] && [ $OFFSET -ne 64 ];         then echo " Phred offset should be 33 or 64 (option -p): $OFFSET" >&2 ; exit 1 ; fi
if ! [[ $NTHREADS =~ ^[0-9]+$ ]];                       then echo " incorrect value (option -t): $NTHREADS"               >&2 ; exit 1 ; fi
if ! [[ "rRfFsStTlLpP" =~ $OUT ]] || [ ${#OUT} -ne 1 ]; then echo " incorrect value (option -v): $OUT"                    >&2 ; exit 1 ; fi

if $CHECK
then
  UNAVAILABLE="\e[0;31m\e[5m[unavailable]\e[25m\e[0m";
  echo ">> checking $AWK:";
  [ ! $(command -v $AWK) ]     && echo -e "$UNAVAILABLE" || { $AWK -Wversion 2>/dev/null || $AWK --version ; } 2>&1 | head -1 ;
  echo ">> checking $GZIP (.gz|.gzip):";
  [ ! $(command -v $GZIP) ]    && echo -e "$UNAVAILABLE" || $GZIP    --version                      </dev/null 2>&1 | head -1 ;
  echo ">> checking $PGZIP (.gz|.gzip):";
  [ ! $(command -v $PGZIP) ]   && echo -e "$UNAVAILABLE" || $PGZIP   --version                      </dev/null 2>&1 | head -1 ;
  echo ">> checking $BZIP (.bz|.bz2|.bzip|.bzip2):";
  [ ! $(command -v $BZIP) ]    && echo -e "$UNAVAILABLE" || $BZIP    --version                      </dev/null 2>&1 | head -1 ;
  echo ">> checking $PBZIP (.bz|.bz2|.bzip|.bzip2):";
  [ ! $(command -v $PBZIP) ]   && echo -e "$UNAVAILABLE" || $PBZIP   --version                      </dev/null 2>&1 | head -1 ;
  echo ">> checking $DSRC (.dsrc|.dsrc2):";
  [ ! $(command -v $DSRC) ]    && echo -e "$UNAVAILABLE" || $DSRC    -s                             </dev/null 2>&1 | head -2 | sed 1d ;
  echo ">> checking $FQZCOMP (.fqz):";
  [ ! $(command -v $FQZCOMP) ] && echo -e "$UNAVAILABLE" || $FQZCOMP -h                             </dev/null 2>&1 | head -1 ;
  echo ">> checking $QUIP (.qp):";
  [ ! $(command -v $QUIP) ]    && echo -e "$UNAVAILABLE" || $QUIP    --version                      </dev/null 2>&1 | head -1 ;
  echo ;
fi


##############################################################################################################
#                                                                                                            #
# INIT                                                                                                       #
#                                                                                                            #
##############################################################################################################
[ $NTHREADS -lt 1 ] && NTHREADS=1;
[ $NTHREADS -gt $(nproc) ] && NTHREADS=$(nproc);
[ $NTHREADS -gt 2 ] && let NTHREADS--;
[ "$OUT" == "t" ] && RESFREQ=0;
TMP=$(mktemp -t -p ${TMPDIR:-/tmp});
trap "disown -a ; kill -9 $(jobs -pr) &> /dev/null ; rm -f $TMP1 $TMP2 &>/dev/null ; exit " SIGINT ;



#############################################################################################################
####                                                                                                     ####
#### FASTQ INFO                                                                                          ####
####                                                                                                     ####
#############################################################################################################
[[ "sS" =~ $OUT ]] && echo -e "#File\tNR\tNB\tAL\tBQ1\tBQ2\tBQ3\tRQ1\tRQ2\tRQ3\tEQ1\tEQ2\tEQ3" ;

for INFILE in "$@"
do
  if [ ! -e $INFILE ]; then echo "[WARNING] not exist; skipping $INFILE"          >&2 ; continue ; fi
  if [   -d $INFILE ]; then echo "[WARNING] not a file; skipping $INFILE"         >&2 ; continue ; fi
  if [ ! -f $INFILE ]; then echo "[WARNING] not regular; skipping $INFILE"        >&2 ; continue ; fi
  if [ ! -r $INFILE ]; then echo "[WARNING] no read permission; skipping $INFILE" >&2 ; continue ; fi

  EXT=${INFILE##*.}; EXT=${EXT^^};
  if   [ "$EXT" == "FASTQ" ] || [ "$EXT" == "FQ" ] ################################################################################ fastq
  then                                                                                 READFILE="cat                   $INFILE";
  elif [ "$EXT" == "GZIP" ]  || [ "$EXT" == "GZ" ] ################################################################################ fastq.gz
  then
    if   [ $NTHREADS -eq 1 ]
    then                                                                               READFILE="$GUNZIP               $INFILE ";
    elif [ ! $(command -v $PGZIP) ]
    then echo "[WARNING] $PGZIP non available; using $GZIP on 1 thread instead" >&2 ;  READFILE="$GUNZIP               $INFILE";
    else                                                                               READFILE="$PGUNZIP -p $NTHREADS $INFILE"; fi
  elif [ "$EXT" == "BZIP" ]  || [ "$EXT" == "BZ" ] || [ "$EXT" == "BZIP2" ] || [ "$EXT" == "BZ2" ]  ############################### fastq.bz2
  then
    if   [ $NTHREADS -eq 1 ]
    then                                                                               READFILE="$BUNZIP               $INFILE";
    elif [ ! $(command -v $PBZIP) ]
    then echo "[WARNING] $PBZIP non available; using $BZIP on 1 thread instead" >&2 ;  READFILE="$BUNZIP               $INFILE";
    else                                                                               READFILE="$PBUNZIP -p $NTHREADS $INFILE"; fi
  elif [ "$EXT" == "DSRC" ] || [ "$EXT" == "DSRC2" ] ############################################################################## fastq.dsrc2
  then
    if   [ ! $(command -v $DSRC) ]
    then echo "[WARNING] $DSRC non available; skipping $INFILE" >&2 ;                  continue ;
    else                                                                               READFILE="$UNDSRC -t$NTHREADS   $INFILE"; fi
  elif [ "$EXT" == "FQZ" ] ######################################################################################################## fastq.fqz
  then
    if   [ ! $(command -v $FQZCOMP) ]
    then echo "[WARNING] $FQZCOMP non available; skipping $INFILE" >&2 ;               continue ;
    elif [ $NTHREADS -eq 1 ]
    then                                                                               READFILE="$UNFQZCOMP -P         $INFILE";
    else                                                                               READFILE="$UNFQZCOMP            $INFILE"; fi
  elif [ "$EXT" == "QP" ] ######################################################################################################### fastq.qp
  then
    if [ ! $(command -v $QUIP) ]
    then echo "[WARNING] $QUIP non available; skipping $INFILE" >&2 ;                  continue ;
    else                                                                               READFILE="$UNQUIP             $INFILE";   fi
  else echo "[WARNING] unrecognized file extension; skipping $INFILE" >&2 ;            continue;
  fi
  $DOS2UNIX && READFILE="$READFILE | tr -d '\r'" ;

  $READFILE 2>/dev/null | $FQREPORT -f $(basename $INFILE) -p $OFFSET > $TMP ;

  case $OUT in
   r|R) echo "##File: $(basename $INFILE)" ; sed 1d $TMP | cut -c1-64 ; echo ;;
   f|F) cat $TMP ;                                                      echo ;;
   s|S) NR="$(head $TMP | grep -F "(NR)" | sed 's/.* //')";
        NB="$(head $TMP | grep -F "(NB)" | sed 's/.* //')";
        AL="$(head $TMP | grep -F "(AL)" | sed 's/.* //')";
        BQ123="$(tail $TMP | grep -F "(B)" | cut -c48-64 | tr -s ' ' | sed 's/^ //' | tr ' ' '\t')";
        RQ123="$(tail $TMP | grep -F "(R)" | cut -c48-64 | tr -s ' ' | sed 's/^ //' | tr ' ' '\t')";
        EQ123="$(tail $TMP | grep -F "(E)" | cut -c48-64 | tr -s ' ' | sed 's/^ //' | tr ' ' '\t')";
        echo -e "$INFILE\t$NR\t$NB\t$AL\t$BQ123\t$RQ123\t$EQ123" ;
        ;;
   t|T) tr -d '#:' < $TMP | sed '/--/d;s/Q=   /Q=/g;s/ \+/\t/g;s/\t\./\t/g' ;;
   l|L|p|P)
      if [ "$OUT" == "l" ]; then LANDSCAPE=true; else LANDSCAPE=false; fi
      N=$(grep -c "^[123456789]" $TMP);       ## no. rows
      QMAX=$(awk '/^0/{print(NF-12)}' $TMP);  ## max Q
      W=40; [ $W -lt $QMAX ] && W=$QMAX;      ## Phred width
      WIDTH=$(( 4090 + 100 * $W ));           ## image width
      HEIGTH=$(( 700 + $N * 100 ));           ## image height
      if $LANDSCAPE
      then echo -e "<svg version=\"1.1\" width=\"$(( $HEIGTH / 10 ))\" height=\"$(( $WIDTH / 10 ))\" xmlns=\"http://www.w3.org/2000/svg\">\n" ;
	   echo -e " <g transform=\"scale(0.1) translate(0, $WIDTH) rotate(270)\">\n" ;
      else echo -e "<svg version=\"1.1\" width=\"$(( $WIDTH / 10 ))\" height=\"$(( $HEIGTH / 10 ))\" xmlns=\"http://www.w3.org/2000/svg\">\n" ;
	   echo -e " <g transform=\"scale(0.1)\">\n" ;
      fi
      ################################################################################
      echo -e "  <!-- background -->\n" ;
      ################################################################################
      echo -e "  <rect width=\"$WIDTH\" height=\"$HEIGTH\" fill=\"white\" />\n" ;
      ### colored strips
      echo -e "  <rect x=\"298\" y=\"200\" width=\"$(( 3604 + 100 * $W ))\" height=\"100\" fill=\"ghostwhite\"/>\n" ;
      for n in $(seq 1 2 $N)
      do y=$(( 300 + 100 * $n ));
	 echo -e "  <rect x=\"300\"  y=\"$y\" width=\"1000\" height=\"100\" fill=\"whitesmoke\"/>\n" ;
	 echo -e "  <rect x=\"1500\" y=\"$y\" width=\"1000\" height=\"100\" fill=\"whitesmoke\"/>\n" ;
	 echo -e "  <rect x=\"2700\" y=\"$y\" width=\"1000\" height=\"100\" fill=\"whitesmoke\"/>\n" ;
	 echo -e "  <rect x=\"3900\" y=\"$y\" width=\"1300\" height=\"100\" fill=\"lavenderblush\"/>\n" ;
	 echo -e "  <rect x=\"5200\" y=\"$y\" width=\"700\"  height=\"100\" fill=\"seashell\"/>\n" ;
	 echo -e "  <rect x=\"5900\" y=\"$y\" width=\"1000\" height=\"100\" fill=\"lightyellow\"/>\n" ;
	 echo -e "  <rect x=\"6900\" y=\"$y\" width=\"$(( 100 * ( $W - 30 ) ))\" height=\"100\" fill=\"honeydew\"/>\n" ;
      done
      ### first column
      for n in $(seq 0 $N)
      do y=$(( 380 + 100 * $n ));
	 echo -e "  <text x=\"40\" y=\"$y\" font-size=\"90\" text-anchor=\"left\" fill=\"darkslategray\" $FONTS>$n</text>\n" ;
      done
      ### file name
      if $LANDSCAPE
      then echo -n "  <text x=\"0\" y=\"0\" transform=\"rotate(90, 20, 120) translate($(( $HEIGTH / 2 )), $(( 260 - $WIDTH )))\" " ;
      else echo -n "  <text x=\"$(( $WIDTH / 2 ))\" y=\"120\" " ;
      fi
      line="$(LC_ALL=en_US.UTF-8 $AWK '(NR==1){printf$2"&#160;&#160;&#160;&#160;&#160;&#160;";next}
                                       (NR==2){printf("no. reads: %\047d", $2);next}
                                       (NR==3){printf("&#160;&#160;&#160;&#160;&#160;&#160;no. bases: %\047d", $2);quit}' $TMP)";
      echo -e "font-size=\"90\" text-anchor=\"middle\" fill=\"darkolivegreen\" font-weight=\"bold\" $FONTS>$line</text>\n" ;
      ################################################################################
      echo -e "  <!-- length -->\n" ;
      ################################################################################
      echo -e "  <text x=\"800\" y=\"275\" font-size=\"90\" text-anchor=\"middle\" fill=\"darkslategray\" $FONTS>read length</text>\n" ;
      ### axes
      y=$(( 400 + $N * 100 ));
      for x in $(seq 300 100 1300)
      do echo -n "  <line x1=\"$x\" x2=\"$x\" y1=\"300\" y2=\"$y\" stroke=\"slategrey\" " ;
	 case $x in
	  300|1300) echo -e "stroke-width=\"5\"/>\n" ;;
	  800)      echo -e "stroke-width=\"3\"/>\n" ;;
	  *)        echo -e "stroke-width=\"1\"/>\n" ;;
	 esac
      done
      y=$(( 475 + $N * 100 ));
      if $LANDSCAPE
      then echo -e "  <text x=\"300\"  y=\"$y\" font-size=\"90\" text-anchor=\"left\" fill=\"darkslategray\" $FONTS transform=\"rotate(90, 300, $y) translate(-65, 25)\">0</text>\n" ;
	   echo -e "  <text x=\"1300\" y=\"$y\" font-size=\"90\" text-anchor=\"left\" fill=\"darkslategray\" $FONTS transform=\"rotate(90, 1300, $y) translate(-65, 25)\">100%</text>\n" ;
      else echo -e "  <text x=\"300\"  y=\"$y\" font-size=\"90\" text-anchor=\"middle\" fill=\"darkslategray\" $FONTS>0</text>\n" ;
	   echo -e "  <text x=\"1300\" y=\"$y\" font-size=\"90\" text-anchor=\"middle\" fill=\"darkslategray\" $FONTS>100%</text>\n" ;
      fi
      ### histogram plot
      $AWK '/^[123456789]/{if($2!="."){print"  <rect x=\"300\" y=\""(305+100*$1)"\" width=\""int(10*$2)"\" height=\"90\" fill=\"royalblue\"/>";print""}}' $TMP ;
      ################################################################################
      echo -e "  <!-- base frequencies -->\n" ;
      ################################################################################
      echo -e "  <text x=\"2000\" y=\"275\" font-size=\"90\" text-anchor=\"middle\" fill=\"darkslategray\" $FONTS>base frequencies</text>\n" ;
      ### axes
      y=$(( 400 + $N * 100 ));
      for x in $(seq 1500 100 2500)
      do echo -n "  <line x1=\"$x\" x2=\"$x\" y1=\"300\" y2=\"$y\" stroke=\"slategrey\" " ;
	 case $x in
	  1500|2500) echo -e "stroke-width=\"5\"/>\n" ;;
	  2000)      echo -e "stroke-width=\"3\"/>\n" ;;
	  *)         echo -e "stroke-width=\"1\"/>\n" ;;
	 esac
      done
      y=$(( 410 + $N * 100 ));
      echo -e "  <rect x=\"1720\" y=\"$y\" width=\"560\" height=\"80\" stroke-width=\"1\" fill=\"ghostwhite\" stroke=\"gray\"/>\n" ;
      y=$(( 475 + $N * 100 ));
      if $LANDSCAPE
      then echo -e "  <text x=\"1500\" y=\"$y\" font-size=\"90\" text-anchor=\"left\" fill=\"darkslategray\" $FONTS transform=\"rotate(90, 1500, $y) translate(-65, 25)\">0</text>\n" ;
	   echo -e "  <text x=\"2500\" y=\"$y\" font-size=\"90\" text-anchor=\"left\" fill=\"darkslategray\" $FONTS transform=\"rotate(90, 2500, $y) translate(-65, 25)\">100%</text>\n" ;
      else echo -e "  <text x=\"1500\" y=\"$y\" font-size=\"90\" text-anchor=\"middle\" fill=\"darkslategray\" $FONTS>0</text>\n" ;
	   echo -e "  <text x=\"2500\" y=\"$y\" font-size=\"90\" text-anchor=\"middle\" fill=\"darkslategray\" $FONTS>100%</text>\n" ;
      fi
      ### pN
      pts="$($AWK 'BEGIN{y=450}/^[123456789]/{if($7!=".")printf" "(1500+int(10*$7))" "y;y+=100}' $TMP)"; 
      echo -e "  <polyline points=\"$pts\" stroke-linecap=\"round\" stroke-linejoin=\"round\" fill=\"none\" stroke-width=\"19\" stroke=\"gray\"/>\n" ;
      if $LANDSCAPE
      then echo -e "  <text x=\"1800\" y=\"$y\" font-size=\"70\" text-anchor=\"left\" fill=\"gray\" $FONTM transform=\"rotate(90, 1800, $y) translate(-48, 25)\">N</text>\n" ;
      else echo -e "  <text x=\"1800\" y=\"$y\" font-size=\"70\" text-anchor=\"middle\" fill=\"gray\" $FONTM>N</text>\n" ;
      fi
      ### pA
      pts="$($AWK 'BEGIN{y=450}/^[123456789]/{if($3!=".")printf" "(1500+int(10*$3))" "y;y+=100}' $TMP)"; 
      echo -e "  <polyline points=\"$pts\" stroke-linecap=\"round\" stroke-linejoin=\"round\" fill=\"none\" stroke-width=\"19\" stroke=\"limegreen\"/>\n" ;
      if $LANDSCAPE
      then echo -e "  <text x=\"2200\" y=\"$y\" font-size=\"70\" text-anchor=\"left\" fill=\"limegreen\" $FONTM transform=\"rotate(90, 2200, $y) translate(-48, 25)\">A</text>\n" ;
      else echo -e "  <text x=\"1900\" y=\"$y\" font-size=\"70\" text-anchor=\"middle\" fill=\"limegreen\" $FONTM>A</text>\n" ;
      fi
      ### pT
      pts="$($AWK 'BEGIN{y=450}/^[123456789]/{if($6!=".")printf" "(1500+int(10*$6))" "y;y+=100}' $TMP)"; 
      echo -e "  <polyline points=\"$pts\" stroke-linecap=\"round\" stroke-linejoin=\"round\" fill=\"none\" stroke-width=\"19\" stroke=\"tomato\"/>\n" ;
      if $LANDSCAPE
      then echo -e "  <text x=\"1900\" y=\"$y\" font-size=\"70\" text-anchor=\"left\" fill=\"tomato\" $FONTM transform=\"rotate(90, 1900, $y) translate(-48, 25)\">T</text>\n" ;
      else echo -e "  <text x=\"2200\" y=\"$y\" font-size=\"70\" text-anchor=\"middle\" fill=\"tomato\" $FONTM>T</text>\n" ;
      fi
      ### pG
      pts="$($AWK 'BEGIN{y=450}/^[123456789]/{if($5!=".")printf" "(1500+int(10*$5))" "y;y+=100}' $TMP)"; 
      echo -e "  <polyline points=\"$pts\" stroke-linecap=\"round\" stroke-linejoin=\"round\" fill=\"none\" stroke-width=\"19\" stroke=\"maroon\"/>\n" ;
      if $LANDSCAPE
      then echo -e "  <text x=\"2000\" y=\"$y\" font-size=\"70\" text-anchor=\"left\" fill=\"maroon\" $FONTM transform=\"rotate(90, 2000, $y) translate(-48, 25)\">G</text>\n" ;
      else echo -e "  <text x=\"2100\" y=\"$y\" font-size=\"70\" text-anchor=\"middle\" fill=\"maroon\" $FONTM>G</text>\n" ;
      fi
      ### pC
      pts="$($AWK 'BEGIN{y=450}/^[123456789]/{if($4!=".")printf" "(1500+int(10*$4))" "y;y+=100}' $TMP)"; 
      echo -e "  <polyline points=\"$pts\" stroke-linecap=\"round\" stroke-linejoin=\"round\" fill=\"none\" stroke-width=\"19\" stroke=\"dodgerblue\"/>\n" ;
      if $LANDSCAPE
      then echo -e "  <text x=\"2100\" y=\"$y\" font-size=\"70\" text-anchor=\"left\" fill=\"dodgerblue\" $FONTM transform=\"rotate(90, 2100, $y) translate(-48, 25)\">C</text>\n" ;
      else echo -e "  <text x=\"2000\" y=\"$y\" font-size=\"70\" text-anchor=\"middle\" fill=\"dodgerblue\" $FONTM>C</text>\n" ;
      fi
      ################################################################################
      echo -e "  <!-- errors -->\n" ;
      ################################################################################
      echo -e "  <text x=\"3200\" y=\"275\" font-size=\"90\" text-anchor=\"middle\" fill=\"darkslategray\" $FONTS>no. error(s) per read</text>\n" ;
      ### axes
      y=$(( 400 + $N * 100 ));
      for x in $(seq 2700 100 3700)
      do echo -n "  <line x1=\"$x\" x2=\"$x\" y1=\"300\" y2=\"$y\" stroke=\"slategrey\" " ;
	 case $x in
	  2700|3700) echo -e "stroke-width=\"5\"/>\n" ;;
	  3200)      echo -e "stroke-width=\"3\"/>\n" ;;
	  *)         echo -e "stroke-width=\"1\"/>\n" ;;
	 esac
      done
      y=$(( 475 + $N * 100 ));
      if $LANDSCAPE
      then echo -e "  <text x=\"2700\" y=\"$y\" font-size=\"90\" text-anchor=\"left\" fill=\"darkslategray\" $FONTS transform=\"rotate(90, 2700, $y) translate(-65, 25)\">0</text>\n" ;
	   echo -e "  <text x=\"3700\" y=\"$y\" font-size=\"90\" text-anchor=\"left\" fill=\"darkslategray\" $FONTS transform=\"rotate(90, 3700, $y) translate(-65, 25)\">100%</text>\n" ;
      else echo -e "  <text x=\"2700\" y=\"$y\" font-size=\"90\" text-anchor=\"middle\" fill=\"darkslategray\" $FONTS>0</text>\n" ;
	   echo -e "  <text x=\"3700\" y=\"$y\" font-size=\"90\" text-anchor=\"middle\" fill=\"darkslategray\" $FONTS>100%</text>\n" ;
      fi
      ### histogram plot
      $AWK '/^[0123456789]/{if($8!="."){print"  <rect x=\"2700\" y=\""(305+100*$1)"\" width=\""int(10*$8)"\" height=\"90\" fill=\"orangered\"/>";print""}}' $TMP ;
      ################################################################################
      echo -e "  <!-- Phred scores -->\n" ;
      ################################################################################
      echo -e "  <text x=\"$(( 3900 + 50 * $W ))\" y=\"275\" font-size=\"90\" text-anchor=\"middle\" fill=\"darkslategray\" $FONTS>Phred quality scores</text>\n" ;
      y=$(( 400 + $N * 100 ));
      echo -e "  <line x1=\"3900\" x2=\"3900\" y1=\"300\" y2=\"$y\" stroke=\"slategrey\" stroke-width=\"5\"/>\n" ;
      echo -e "  <line x1=\"5200\" x2=\"5200\" y1=\"300\" y2=\"$y\" stroke=\"slategrey\" stroke-width=\"1\"/>\n" ;
      echo -e "  <line x1=\"5900\" x2=\"5900\" y1=\"300\" y2=\"$y\" stroke=\"slategrey\" stroke-width=\"1\"/>\n" ;
      echo -e "  <line x1=\"6900\" x2=\"6900\" y1=\"300\" y2=\"$y\" stroke=\"slategrey\" stroke-width=\"1\"/>\n" ;
      x=$(( 3900 + 100 * $W ));
      echo -e "  <line x1=\"$x\" x2=\"$x\" y1=\"300\" y2=\"$y\" stroke=\"slategrey\" stroke-width=\"5\"/>\n" ;
      y=$(( 475 + $N * 100 ));
      if $LANDSCAPE
      then echo -e "  <text x=\"3900\" y=\"$y\" font-size=\"90\" text-anchor=\"left\" fill=\"darkslategray\" $FONTS transform=\"rotate(90, 3900, $y) translate(-65, 25)\">0</text>\n" ;
	   echo -e "  <text x=\"5200\" y=\"$y\" font-size=\"90\" text-anchor=\"left\" fill=\"darkslategray\" $FONTS transform=\"rotate(90, 5200, $y) translate(-65, 25)\">13</text>\n" ;
	   echo -e "  <text x=\"5900\" y=\"$y\" font-size=\"90\" text-anchor=\"left\" fill=\"darkslategray\" $FONTS transform=\"rotate(90, 5900, $y) translate(-65, 25)\">20</text>\n" ;
	   echo -e "  <text x=\"6900\" y=\"$y\" font-size=\"90\" text-anchor=\"left\" fill=\"darkslategray\" $FONTS transform=\"rotate(90, 6900, $y) translate(-65, 25)\">30</text>\n" ;
	   echo -e "  <text x=\"$x\" y=\"$y\" font-size=\"90\" text-anchor=\"left\" fill=\"darkslategray\" $FONTS transform=\"rotate(90, $x, $y) translate(-65, 25)\">$W</text>\n" ;
      else echo -e "  <text x=\"3900\" y=\"$y\" font-size=\"90\" text-anchor=\"middle\" fill=\"darkslategray\" $FONTS>0</text>\n" ;
	   echo -e "  <text x=\"5200\" y=\"$y\" font-size=\"90\" text-anchor=\"middle\" fill=\"darkslategray\" $FONTS>13</text>\n" ;
	   echo -e "  <text x=\"5900\" y=\"$y\" font-size=\"90\" text-anchor=\"middle\" fill=\"darkslategray\" $FONTS>20</text>\n" ;
	   echo -e "  <text x=\"6900\" y=\"$y\" font-size=\"90\" text-anchor=\"middle\" fill=\"darkslategray\" $FONTS>30</text>\n" ;
	   echo -e "  <text x=\"$x\" y=\"$y\" font-size=\"90\" text-anchor=\"middle\" fill=\"darkslategray\" $FONTS>$W</text>\n" ;
      fi
      ### histogram plot
      $AWK '/^[123456789]/{cdf=0;c1=c2=q=-1;x=11;while(++x<=NF){++q;cdf+=$x;if(c1<0&&cdf>=2.5)c1=q;if(c2<0&&cdf>=97.5)c2=q}q1=$9;q2=$10;q3=$11;iq=q3-q1;
			   print"  <rect x=\""(3900+100*q1)"\" y=\""(315+100*$1)"\" width=\""((iq==0)?1:100*iq)"\" height=\"70\" rx=\"15\" ry=\"1\" stroke-width=\"15\" fill=\"none\" stroke=\"slategray\"/>";print"";
                           print"  <line x1=\""(3900+100*c1)"\" x2=\""(3900+100*q1)"\" y1=\""(350+100*$1)"\" y2=\""(350+100*$1)"\" stroke-linecap=\"round\" stroke-width=\"6\" stroke=\"slategray\"/>";       print"";
                           print"  <line x1=\""(3900+100*q3)"\" x2=\""(3900+100*c2)"\" y1=\""(350+100*$1)"\" y2=\""(350+100*$1)"\" stroke-linecap=\"round\" stroke-width=\"6\" stroke=\"slategray\"/>";       print"";
                           print"  <line x1=\""(3900+100*q2)"\" x2=\""(3900+100*q2)"\" y1=\""(322+100*$1)"\" y2=\""(378+100*$1)"\" stroke-linecap=\"butt\" stroke-width=\"13\" stroke=\"orangered\"/>";       print""}' $TMP ;
      echo -e " </g>\n" ;
      echo "</svg>" ;
      ;;       
  esac
  
  echo -n > $TMP ;
done

rm -f $TMP ;

exit ;
 
